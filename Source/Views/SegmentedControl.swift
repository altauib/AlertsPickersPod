import UIKit

open class SegmentedControl: UISegmentedControl {
    
    public typealias Action = (Int) -> Swift.Void
    
    fileprivate var action: Action?
    
    public func action(new: Action?) {
        if action == nil {
            addTarget(self, action: #selector(segmentedControlValueChanged(segment:)), for: .valueChanged)
        }
        action = new
    }
    
    @objc public func segmentedControlValueChanged(segment: UISegmentedControl) {
        action?(segment.selectedSegmentIndex)
    }
}
