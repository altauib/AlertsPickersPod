// swift-tools-version:5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "AlertsPickers",
    products: [
       .library(name: "AlertsPickers", targets: ["AlertsPickers"])
   ],
   targets: [
       .target(
           name: "AlertsPickers",
           path: "Source",
           resources: [.copy("Pickers/Locale/Countries.bundle")]
       )
   ]
)
